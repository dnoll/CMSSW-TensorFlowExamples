# `GraphLoading` example

### Single-threaded example

```bash
cd $CMSSW_BASE/src
git clone https://gitlab.cern.ch/dnoll/CMSSW-TensorFlowExamples.git TensorFlowExamples
scram b
cmsRun TensorFlowExamples/GraphLoading/test/graphLoading_cfg.py
```

# -*- coding: utf-8 -*-

"""
Test config to run the GraphLoading example.
"""


import os
import subprocess

import FWCore.ParameterSet.Config as cms
from FWCore.ParameterSet.VarParsing import VarParsing


# helper to determine the location if _this_ file
def get_this_dir():
    if "__file__" in globals():
        return os.path.dirname(os.path.abspath(__file__))
    else:
        return os.path.expandvars(
            "$CMSSW_BASE/src/TensorFlowExamples/GraphLoading/test"
        )


# ensure that the graph exists
# if not, call the create_graph.py script in a subprocess since tensorflow complains
# when its loaded twice (once here in python, once in c++)
graph_path = os.path.abspath("TensorFlowExamples/graph.pb")

# setup minimal options
options = VarParsing("python")
options.setDefault(
    "inputFiles",
    "root://cms-xrd-global.cern.ch///store/mc/RunIIFall17MiniAODv2/GluGluToHHTo2B2VTo2L2Nu_node_SM_13TeV-madgraph_correctedcfg/MINIAODSIM/PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/10000/EC1B2B42-F5AF-E811-84F1-ECB1D79E5C40.root",
)
options.parseArguments()

# define the process to run
process = cms.Process("TF")

# minimal configuration
process.load("FWCore.MessageService.MessageLogger_cfi")
process.MessageLogger.cerr.FwkReport.reportEvery = 1
process.maxEvents = cms.untracked.PSet(input=cms.untracked.int32(1))
process.source = cms.Source(
    "PoolSource", fileNames=cms.untracked.vstring(options.inputFiles)
)

# process options
process.options = cms.untracked.PSet(
    allowUnscheduled=cms.untracked.bool(True), wantSummary=cms.untracked.bool(True)
)

# load the graphLoading example module
process.load("TensorFlowExamples.GraphLoading.graphLoading_cfi")
process.graphLoading.graphPath = cms.string(graph_path)

# define the path to run
process.p = cms.Path(process.graphLoading)
